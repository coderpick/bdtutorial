<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware'=>'ipCheck'],function () {

    /***********************
     *  Frontend Route list
    ***********************/

    Route::group(['namespace'=>'Frontend'],function(){
        Route::get('/',[
            'as'=>'Home',
            'uses'=>'HomeController@index'
        ]);

        Route::get('gallery',[
            'as'=>'gallery',
            'uses'=>'HomeController@gallery'
        ]);

        Route::get('search',[
            'as'=>'search.gallery',
            'uses'=>'HomeController@searchGallery'
        ]);

        Route::get('searchByDivision',[
            'as'=>'galleryBydivision',
            'uses'=>'HomeController@searchGalleryByDivision'
        ]);

        Route::get('district',[
            'as'   =>'search.district',
            'uses' =>'HomeController@getDistrict'
        ]);

        Route::get('searchBydistrict',[
            'as'=>'galleryBydistrict',
            'uses'=>'HomeController@searchGalleryByDistrict'
        ]);

        Route::get('subdistrict',[
            'as'   =>'search.subdistrict',
            'uses' =>'HomeController@getSubDistrict'
        ]);

        Route::get('searchBysubdistrict',[
            'as'=>'searchBysubdistrict',
            'uses'=>'HomeController@searchGalleryBysubDistrict'
        ]);


        Route::get('login',[
            'as' =>'login.form',
            'uses'=>'HomeController@showLoginForm'

        ]);

        Route::get('registration',[
            'as' =>'registration',
            'uses'=>'HomeController@showRegistrationForm'

        ]);
        Route::post('user/add',[
            'as' => 'add.user',
            'uses'=> 'HomeController@register'
        ]);

        Route::get('resource',[
            'as'=>'resource',
            'uses'=>'HomeController@showresourcePage'
        ]);
        Route::get('historical-place',[
            'as'=>'historicalplace.list',
            'uses'=>'HomeController@historicalplace'
        ]);
        Route::get('historical-place/{slug}',[
            'as'=>'historicalplace.details',
            'uses'=>'HomeController@historicalplaceDetails'
        ]);

        Route::get('institutions',[
            'as'=>'institution',
            'uses'=>'HomeController@institution'
        ]);
        Route::get('institutions/{type}',[
            'as'=>'institution.type',
            'uses'=>'HomeController@institutionType'
        ]);
        Route::get('institution/{id}/{slug}',[
            'as'=>'institution.details',
            'uses'=>'HomeController@institutiondetails'
        ]);
        Route::get('institution/news/{id}/{slug}',[
            'as'=>'institution.news.details',
            'uses'=>'HomeController@institutionNewsDetails'
        ]);
        /* Tutors routs */
        Route::get('tutors',[
            'as'=>'tutors',
            'uses'=>'TutorController@index'
        ]);

        Route::get('tutors-info/{id}/{slug}',[
            'as'=>'tutor.info',
            'uses'=>'TutorController@show'
        ]);

        /* Add rating rout list */
        Route::post('tutors-info',[
            'as' =>'addRating',
            'uses'=>'RatingController@store'
        ]);

        /* quick learn route for frontend*/
       Route::get('quick-learn',[
            'as'=>'quick.learn',
            'uses'=>'HomeController@QuickLearn'

       ]);

       Route::get('category/{slug}',[
           'as' =>'quicklearn.details',
           'uses'=>'TipsController@index'
       ]);
       /* E-book front route */
        Route::get('books/categories',[
            'as' =>'ebooks',
            'uses'=>'EbookController@index'
        ]);
        Route::get('books/category/{id}/{slug}',[
            'as' =>'ebook.category',
            'uses'=>'EbookController@bookCategory'
        ]);
        Route::get('books/{slug}',[
            'as' =>'ebook.info',
            'uses'=>'EbookController@bookinof'
        ]);

        include('rejvi_front_route.php');


    });


    Route::group(['prefix' => 'admin','middleware' => 'authCheck', 'namespace' => 'Admin'], function () {

        Route::get('dashboard', [
            'as'=>'dashboard',
            'uses'=>'DashboardController@index',
            'title' => 'Dashboard'

        ]);

        Route::get('user/addUser', [
            'as' => 'addUser',
            'uses' => 'UserController@create',
            'title' => 'Add Users'

        ]);
        Route::post('user/storeUser', [
            'as' => 'user.store',
            'uses' => 'UserController@store',
            'title' => 'Store Users'

        ]);

        Route::get('user/usersList', [
            'as' => 'user.index',
            'uses' => 'UserController@index',
            'title' => 'List Users'


        ]);


//user edit
        Route::get('user/{id}/edit', [
            'as' => 'edit_user.edit',
            'uses' => 'UserController@edit',
            'title' => 'Edit Users'
        ]);  //user change Password


        Route::get('user/profile', [
            'as' => 'profile',
            'uses' => 'UserController@changeProfile',
            'title' => 'User Profile'


        ]);
        Route::put('user/Update_profile/{id}', [
            'as' => 'update.profile',
            'uses' => 'UserController@updateProfile',
            'title' => 'Update Users Profile'
        ]);
//user update
        Route::put('user/{id}/edit', [
            'as' => 'user.update',
            'uses' => 'UserController@update',
            'title' => 'Update Users'

        ]);
//user trash
        Route::get('user/{id}/trash', [
            'as' => 'user.trash',
            'uses' => 'UserController@trash',
            'title' => 'Trash Users'


        ]);
//user restore
        Route::get('user/{id}/restore', [
            'as' => 'user.restore',
            'uses' => 'UserController@restore',
            'title' => 'Restore Users'
        ]);
//user Delete
        Route::get('user/{id}/delete', [
            'as' => 'user.delete',
            'uses' => 'UserController@destroy',
            'title' => 'Delete Users'

        ]);


        Route::get('block_ip',[
            'as'=>'block_ip.index',
            'uses'=>'IpController@index',
            'title'=>'List of blocked IP'
        ]);

        Route::get('block_ip/destroy/{id}',[
            'as'=>'block_ip.destroy',
            'uses'=>'IpController@destroy',
            'title'=>'Delete blocked IP'
        ]);


        include('rejvi.php');

    /* Gallery routes */

        Route::get('gallery/gallerylist',[
            'as'=>'gallery.index',
            'uses'=>'GalleryController@index',
            'title'=>'Show gallery'
        ]);

        Route::get('gallery/add',[
            'as'=>'gallery.add',
            'uses'=>'GalleryController@create',
            'title'=>'Add gallery'
        ]);

        Route::get('getDistrict',[
            'as'=>'findDistrict',
            'uses'=>'GalleryController@getDistrict'
        ]);

        Route::get('getThana',[
            'as'=>'findThana',
            'uses'=>'GalleryController@getThana'
        ]);


        Route::post('gallery/store',[
            'as'=>'gallery.store',
            'uses'=>'GalleryController@store',
            'title'=>'Store gallery'
        ]);

        Route::get('gallery/{id}/edit',[
            'as'=>'gallery.edit',
            'uses'=>'GalleryController@edit'
        ]);

        Route::post('gallery/{id}/update',[
            'as'=>'gallery.update',
            'uses'=>'GalleryController@update'
        ]);

        Route::get('gallery/{id}/delete',[
            'as'=>'gallery.detete',
            'uses'=>'GalleryController@delete'
        ]);

    /* Tutors route section for admin */

        Route::get('teachers',[
            'as'=>'teachers',
            'uses'=>'TeachersController@index'
        ]);

        Route::get('teacher/add',[
            'as'=>'teacher.add',
            'uses'=>'TeachersController@create'
        ]);

        Route::post('teacher.store',[
            'as'=>'teacher.store',
            'uses'=>'TeachersController@store'
        ]);

        Route::get('teacher/{id}/edit',[
            'as'=>'teacher.edit',
            'uses'=>'TeachersController@edit'
        ]);

        Route::post('teacher/{id}/update',[
            'as'=>'teacher.update',
            'uses'=>'TeachersController@update'
        ]);

        Route::get('teacher/{id}/delete',[
            'as'=>'teacher.detete',
            'uses'=>'TeachersController@delete'
        ]);

        /* Add teachers feature at admin*/

        Route::get('teacher/skill/{id}',[
            'as' =>'teacher.skill',
            'uses'=>'TeacherFeatureController@index'
        ]);

        Route::get('teacher/add-skill/{id}',[
            'as' =>'add.skill',
            'uses'=>'TeacherFeatureController@create'
        ]);

        Route::post('teacher-skill-store/{id}',[
            'as'=>'teacher.skill.store',
            'uses'=>'TeacherFeatureController@store'
        ]);

        Route::get('teacher/edit-skill/{id}',[
            'as' =>'teacher.skill.edit',
            'uses'=>'TeacherFeatureController@edit'
        ]);


        Route::post('teacher/edit-skill/{id}/update',[
            'as'=>'teacher.skill.update',
            'uses'=>'TeacherFeatureController@update'
        ]);

        Route::get('teacher/delete-skill/{id}',[
            'as'=>'teacher.skill.delete',
            'uses'=>'TeacherFeatureController@delete'
        ]);

        /* Teacher education route */
        Route::get('teacher/education/{id}',[
            'as' =>'teacher.education',
            'uses'=>'TeacherFeatureController@TeacherEducation'
        ]);

        Route::get('teacher/add-education/{id}',[
            'as' =>'add.education',
            'uses'=>'TeacherFeatureController@addTeacherEducation'
        ]);

        Route::post('teacher-education-store/{id}',[
            'as'=>'teacher.education.store',
            'uses'=>'TeacherFeatureController@storeEducatoin'
        ]);

        Route::get('teacher/edit-education/{id}',[
            'as' =>'teacher.education.edit',
            'uses'=>'TeacherFeatureController@editEducation'
        ]);


        Route::post('teacher/education/{id}/update',[
            'as'=>'teacher.education.update',
            'uses'=>'TeacherFeatureController@updateEducation'
        ]);

        Route::get('teacher/delete-education/{id}',[
            'as'=>'teacher.education.delete',
            'uses'=>'TeacherFeatureController@deleteEducation'
        ]);
        /* Tips Category route for Admin */
        Route::get('category',[
            'as' =>'tips.category.list',
            'uses'=>'TipsCategoryController@index'
        ]);
        Route::get('category/add',[
            'as' =>'tips.category.add',
            'uses'=>'TipsCategoryController@create'
        ]);
        Route::post('category/add',[
            'as' =>'tips.category.store',
            'uses'=>'TipsCategoryController@store'
        ]);
        Route::get('category/{id}/edit',[
            'as' =>'tips.category.edit',
            'uses'=>'TipsCategoryController@edit'
        ]);
        Route::post('category/{id}/update',[
            'as' =>'tips.category.update',
            'uses'=>'TipsCategoryController@update'
        ]);
        Route::get('category/{id}/delete',[
            'as' =>'tips.category.delete',
            'uses'=>'TipsCategoryController@delete'
        ]);

        /* quick tutor routes for admin */

        Route::get('quick-learn',[
            'as' => 'quick.learn.list',
            'uses' => 'QuickLearnController@index'
        ]);
        Route::get('quick-learn/add',[
            'as' => 'quick.learn.add',
            'uses' => 'QuickLearnController@create'
        ]);
        Route::post('quick-learn/store',[
            'as' => 'quick.learn.store',
            'uses' => 'QuickLearnController@store'
        ]);
        Route::get('quick-learn/{id}/edit',[
            'as' => 'quick.learn.edit',
            'uses' => 'QuickLearnController@edit'
        ]);
        Route::post('quick-learn/{id}update',[
            'as' => 'quick.learn.update',
            'uses' => 'QuickLearnController@update'
        ]);
        Route::get('quick-learn/{id}/delete',[
            'as' => 'quick.learn.delete',
            'uses' => 'QuickLearnController@delete'
        ]);

        /* Historical place route for admin */
        Route::get('historical-place',[
            'as' => 'historical.list',
            'uses' => 'HistoricalPlaceController@index'
        ]);
        Route::get('historical-place/add',[
            'as' => 'historical.add',
            'uses' => 'HistoricalPlaceController@create'
        ]);
        Route::post('historical-place/store',[
            'as' => 'historical.store',
            'uses' => 'HistoricalPlaceController@store'
        ]);
        Route::get('historical-place/{id}/edit',[
            'as' => 'historical.edit',
            'uses' => 'HistoricalPlaceController@edit'
        ]);
        Route::post('historical-place/{id}update',[
            'as' => 'historical.update',
            'uses' => 'HistoricalPlaceController@update'
        ]);
        // trash
        Route::get('historical-place/trash/{id}', [
            'as' => 'historical.trash',
            'uses' => 'HistoricalPlaceController@trash',
            'title' => 'Trash historical place'
        ]);
        // restore
        Route::get('historical-place/restore/{id}', [
            'as' => 'historical.restore',
            'uses' => 'HistoricalPlaceController@restore',
            'title' => 'Restore historical place'
        ]);
        Route::get('historical-place/{id}/delete',[
            'as' => 'historical.delete',
            'uses' => 'HistoricalPlaceController@delete'
        ]);
        /* Start institution route for admim  */
        Route::get('institution',[
            'as' => 'institution.list',
            'uses' => 'InstitutionController@index'
        ]);
        Route::get('institution/add',[
            'as' => 'institution.add',
            'uses' => 'InstitutionController@create'
        ]);
        Route::post('institution/store',[
            'as' => 'institution.store',
            'uses' => 'InstitutionController@store'
        ]);
        Route::get('institution/{id}/edit',[
            'as' => 'institution.edit',
            'uses' => 'InstitutionController@edit'
        ]);
        Route::post('institution/{id}update',[
            'as' => 'institution.update',
            'uses' => 'InstitutionController@update'
        ]);
        // trash
        Route::get('institution/trash/{id}', [
            'as' => 'institution.trash',
            'uses' => 'InstitutionController@trash',
        ]);
        // restore
        Route::get('institution/restore/{id}', [
            'as' => 'institution.restore',
            'uses' => 'InstitutionController@restore',
        ]);
        Route::get('institution/{id}/delete',[
            'as' => 'institution.delete',
            'uses' => 'InstitutionController@delete'
        ]);
        /* End institution route */

        /* Start institution Slider */
        Route::get('institution/slider/{id}',[
            'as' => 'institution.slider.list',
            'uses' => 'InstitutionFeatureController@index'
        ]);
        Route::get('institution/slider/add/{id}',[
            'as' => 'institution.slider.add',
            'uses' => 'InstitutionFeatureController@create'
        ]);
        Route::post('institution/slider/store/{id}',[
            'as' => 'institution.slider.store',
            'uses' => 'InstitutionFeatureController@store'
        ]);
        Route::get('institution/slider/{id}/edit',[
            'as' => 'institution.slider.edit',
            'uses' => 'InstitutionFeatureController@edit'
        ]);
        Route::post('institution/slider/{id}/update',[
            'as' => 'institution.slider.update',
            'uses' => 'InstitutionFeatureController@update'
        ]);
        // trash
        Route::get('institution/slider/trash/{id}', [
            'as' => 'institution.slider.trash',
            'uses' => 'InstitutionFeatureController@trash',
        ]);
        // restore
        Route::get('institution/slider/restore/{id}', [
            'as' => 'institution.slider.restore',
            'uses' => 'InstitutionFeatureController@restore',
        ]);
        Route::get('institution/slider/{id}/delete',[
            'as' => 'institution.slider.delete',
            'uses' => 'InstitutionFeatureController@delete'
        ]);
        /* End institution slider route*/

        /***************************************
         * Start institution news route         *
         **************************************/

        Route::get('institution/news/{id}',[
            'as' => 'institution.news.list',
            'uses' => 'InstitutionNewsController@index'
        ]);
        Route::get('institution/news/add/{id}',[
            'as' => 'institution.news.add',
            'uses' => 'InstitutionNewsController@create'
        ]);
        Route::post('institution/news/store/{id}',[
            'as' => 'institution.news.store',
            'uses' => 'InstitutionNewsController@store'
        ]);
        Route::get('institution/news/edit/{id}',[
            'as' => 'institution.news.edit',
            'uses' => 'InstitutionNewsController@edit'
        ]);
        Route::post('institution/news/update{id}',[
            'as' => 'institution.news.update',
            'uses' => 'InstitutionNewsController@update'
        ]);
        // trash
        Route::get('institution/news/trash/{id}', [
            'as' => 'institution.news.trash',
            'uses' => 'InstitutionNewsController@trash',
        ]);
        // restore
        Route::get('institution/news/restore/{id}', [
            'as' => 'institution.news.restore',
            'uses' => 'InstitutionNewsController@restore',
        ]);
        Route::get('institution/news/delete/{id}',[
            'as' => 'institution.news.delete',
            'uses' => 'InstitutionNewsController@delete'
        ]);
        /* End institution news route*/

        /* Start E-book Category Route for admin*/
        Route::get('books/category',[
            'as' => 'book.category.list',
            'uses' => 'EbookCategoryController@index'
        ]);
        Route::get('books/category/add',[
            'as' => 'book.category.add',
            'uses' => 'EbookCategoryController@create'
        ]);
        Route::post('books/category/store',[
            'as' => 'book.category.store',
            'uses' => 'EbookCategoryController@store'
        ]);
        Route::get('books/category/edit/{id}',[
            'as' => 'book.category.edit',
            'uses' => 'EbookCategoryController@edit'
        ]);
        Route::post('books/category/update/{id}',[
            'as' => 'book.category.update',
            'uses' => 'EbookCategoryController@update'
        ]);
        // trash
        Route::get('books/category/trash/{id}', [
            'as' => 'book.category.trash',
            'uses' => 'EbookCategoryController@trash',
        ]);
        // restore
        Route::get('books/category/restore/{id}', [
            'as' => 'book.category.restore',
            'uses' => 'EbookCategoryController@restore',
        ]);
        Route::get('books/category/delete/{id}',[
            'as' => 'book.category.delete',
            'uses' => 'EbookCategoryController@delete'
        ]);
        /* End E-book Category Route for admin*/

        /* Start E-book  Route for admin */
        Route::get('books',[
            'as' => 'book.list',
            'uses' => 'BookController@index'
        ]);
        Route::get('book/add',[
            'as' => 'book.add',
            'uses' => 'BookController@create'
        ]);
        Route::post('book/store',[
            'as' => 'book.store',
            'uses' => 'BookController@store'
        ]);
        Route::get('book/edit/{id}',[
            'as' => 'book.edit',
            'uses' => 'BookController@edit'
        ]);
        Route::post('book/update/{id}',[
            'as' => 'book.update',
            'uses' => 'BookController@update'
        ]);
        // trash
        Route::get('book/trash/{id}', [
            'as' => 'book.trash',
            'uses' => 'BookController@trash',
        ]);
        // restore
        Route::get('book/restore/{id}', [
            'as' => 'book.restore',
            'uses' => 'BookController@restore',
        ]);
        Route::get('book/delete/{id}',[
            'as' => 'book.delete',
            'uses' => 'BookController@delete'
        ]);
        /* End E-book Route for admin*/
    });



    Route::group(['namespace' => 'Admin'], function () {
//login
        Route::get('/admin', [
            'as' => 'login.index',
            'uses' => 'UserAuthController@index',
            'title' => 'Login'
        ]);

//registration
        Route::get('reg/regUser', [
            'as' => 'regUser',
            'uses' => 'UserController@registration',
            'title' => 'Users Registration'

        ]);
        Route::post('reg/storeUser', [
            'as' => 'user.store',
            'uses' => 'UserController@regStore',
            'title' => 'Register Users'

        ]);

        Route::get('verify_email/{id}/{token}', [
            'as' => 'verify_email',
            'uses' => 'UserController@verify_email'
        ]);
        Route::get('resend/{id}', [
            'as' => 'resend',
            'uses' => 'UserController@resend_email'
        ]);



        Route::post('login_check', [
            'as' => 'login_check',
            'uses' => 'UserAuthController@login',
            'title' => 'Check Login'
        ]);
        Route::post('logout', [
            'as' => 'logout',
            'uses' => 'UserAuthController@logout',
            'title' => 'Logout'
        ]);
        ///User Password Reset
        Route::post('reset_user_password', [
            'as' => 'reset_user_password',
            'uses' => 'UserAuthController@reset_user_password',
            'title' => 'Reset Password'
        ]);

        Route::get('password_reset/{id}/{token}', [
            'as' => 'password_reset',
            'uses' => 'UserAuthController@password_reset',
            'title' => 'Reset Password by Email Verification'
        ]);
        Route::put('update_password/{id}', [
            'as' => 'update_password',
            'uses' => 'UserAuthController@update_password',
            'title' => 'Update Password'
        ]);
        // Suspended account
        Route::get('login/{id?}/{token?}', [
            'as' => 'login',
            'uses' => 'UserAuthController@suspended_account_login_panel',
            'title' => 'Login panel for suspended account'
        ]);


        Route::put('login_suspended_account', [
            'as' => 'login_suspended_account',
            'uses' => 'UserAuthController@login_suspended_account',
            'title' => 'Login suspended account'
        ]);
        Route::get('resend_suspend_email/{user_id}', [
            'as' => 'resend_suspend_email',
            'uses' => 'UserAuthController@resend_suspend_email',
            'title' => 'Resend suspended email'
        ]);

    });


});

//Auth::routes();


//Route::get('/home', 'HomeController@index')->name('home');
