<?php

//--------------Start Of Academic Route-------------------


/*Academic Class routes*/
//list
Route::get('academic/class/list', [
'as' => 'class.index',
'uses' => 'ClassController@index',
'title' => 'Class List'


]);
// create
Route::get('academic/class/create', [
'as' => 'class.create',
'uses' => 'ClassController@create',
'title' => 'Add Class'

]);
// store
Route::post('academic/class/store', [
'as' => 'class.store',
'uses' => 'ClassController@store',
'title' => 'Store Class'

]);
// edit
Route::get('academic/class/edit/{id}', [
'as' => 'class.edit',
'uses' => 'ClassController@edit',
'title' => 'Edit Class'
]);
// update
Route::put('academic/class/update/{id}', [
'as' => 'class.update',
'uses' => 'ClassController@update',
'title' => 'Update Class'

]);
// trash
Route::get('academic/class/trash/{id}', [
'as' => 'class.trash',
'uses' => 'ClassController@trash',
'title' => 'Trash Class'


]);
// restore
Route::get('academic/class/restore/{id}', [
'as' => 'class.restore',
'uses' => 'ClassController@restore',
'title' => 'Restore Class'
]);
// delete
Route::get('academic/class/delete/{id}', [
'as' => 'class.delete',
'uses' => 'ClassController@destroy',
'title' => 'Delete Class'

]);

/*Academic Subject routes*/
//list
Route::get('academic/class/subject/list/{slug}', [
'as' => 'class_subject.index',
'uses' => 'ClassSubjectController@index',
'title' => 'Class Subject List'
]);
// create
Route::get('academic/class/subject/create/{slug}', [
'as' => 'class_subject.create',
'uses' => 'ClassSubjectController@create',
'title' => 'Add Class Subject'

]);
// store
Route::post('academic/class/subject/store/{slug}', [
'as' => 'class_subject.store',
'uses' => 'ClassSubjectController@store',
'title' => 'Store Class Subject'

]);
// edit
Route::get('academic/class/subject/edit/{id}', [
    'as' => 'class_subject.edit',
    'uses' => 'ClassSubjectController@edit',
    'title' => 'Update Class Subject'

]);
// update
Route::put('academic/class/subject/update/{id}', [
'as' => 'class_subject.update',
'uses' => 'ClassSubjectController@update',
'title' => 'Update Class Subject'

]);
// trash
Route::get('academic/class/subject/trash/{id}', [
'as' => 'class_subject.trash',
'uses' => 'ClassSubjectController@trash',
'title' => 'Trash Class Subject'


]);
// restore
Route::get('academic/class/subject/restore/{id}', [
'as' => 'class_subject.restore',
'uses' => 'ClassSubjectController@restore',
'title' => 'Restore Class Subject'
]);
// delete
Route::get('academic/class/subject/delete/{id}', [
'as' => 'class_subject.delete',
'uses' => 'ClassSubjectController@destroy',
'title' => 'Delete Class Subject'

]);

/*Academic Chapter routes*/
//list
Route::get('academic/chapter/list/{class}/{subject}', [
    'as' => 'subject_chapter.index',
    'uses' => 'ChapterController@index',
    'title' => 'Chapter List'
]);
// create
Route::get('academic/chapter/create/{class}/{subject}', [
    'as' => 'subject_chapter.create',
    'uses' => 'ChapterController@create',
    'title' => 'Add Chapter'

]);
// store
Route::post('academic/chapter/store/{class}/{subject}', [
    'as' => 'subject_chapter.store',
    'uses' => 'ChapterController@store',
    'title' => 'Store Chapter'

]);
// update
Route::get('academic/chapter/edit/{id}', [
    'as' => 'subject_chapter.edit',
    'uses' => 'ChapterController@edit',
    'title' => 'Update Chapter'

]);
// update
Route::put('academic/chapter/update/{id}', [
    'as' => 'subject_chapter.update',
    'uses' => 'ChapterController@update',
    'title' => 'Update Chapter'

]);
// trash
Route::get('academic/chapter/trash/{id}', [
    'as' => 'subject_chapter.trash',
    'uses' => 'ChapterController@trash',
    'title' => 'Trash Chapter'
]);
// restore
Route::get('academic/chapter/restore/{id}', [
    'as' => 'subject_chapter.restore',
    'uses' => 'ChapterController@restore',
    'title' => 'Restore Chapter'
]);
// delete
Route::get('academic/chapter/delete/{id}', [
    'as' => 'subject_chapter.delete',
    'uses' => 'ChapterController@destroy',
    'title' => 'Delete Chapter'
]);

/*Academic chapter content routes*/
//list
Route::get('academic/cc/list/{class}/{subject}/{chapter}', [
    'as' => 'chapter_content.index',
    'uses' => 'ChapterContentController@index',
    'title' => 'Chapter Content List'
]);
// create
Route::get('academic/cc/create/{class}/{subject}/{chapter}', [
    'as' => 'chapter_content.create',
    'uses' => 'ChapterContentController@create',
    'title' => 'Add Chapter Content'

]);
// store
Route::post('academic/cc/store/{class}/{subject}/{chapter}', [
    'as' => 'chapter_content.store',
    'uses' => 'ChapterContentController@store',
    'title' => 'Store Chapter Content'

]);
// update
Route::get('academic/cc/edit/{id}', [
    'as' => 'chapter_content.edit',
    'uses' => 'ChapterContentController@edit',
    'title' => 'Update Chapter Content'

]);
// update
Route::put('academic/cc/update/{id}', [
    'as' => 'chapter_content.update',
    'uses' => 'ChapterContentController@update',
    'title' => 'Update Chapter Content'

]);
// trash
Route::get('academic/cc/trash/{id}', [
    'as' => 'chapter_content.trash',
    'uses' => 'ChapterContentController@trash',
    'title' => 'Trash Chapter Content'
]);
// restore
Route::get('academic/cc/restore/{id}', [
    'as' => 'chapter_content.restore',
    'uses' => 'ChapterContentController@restore',
    'title' => 'Restore Chapter Content'
]);
// delete
Route::get('academic/cc/delete/{id}', [
    'as' => 'chapter_content.delete',
    'uses' => 'ChapterContentController@destroy',
    'title' => 'Delete Chapter Content'
]);


/*Academic Subject routes*/
//list
Route::get('academic/subject/list', [
'as' => 'subject.index',
'uses' => 'SubjectController@index',
'title' => 'Subject List'
]);
// create
Route::get('academic/subject/create', [
'as' => 'subject.create',
'uses' => 'SubjectController@create',
'title' => 'Add Subject'

]);
// store
Route::post('academic/subject/store', [
'as' => 'subject.store',
'uses' => 'SubjectController@store',
'title' => 'Store Subject'

]);
// edit
Route::get('academic/subject/edit/{id}', [
'as' => 'subject.edit',
'uses' => 'SubjectController@edit',
'title' => 'Edit Subject'
]);
// update
Route::put('academic/subject/update/{id}', [
'as' => 'subject.update',
'uses' => 'SubjectController@update',
'title' => 'Update Subject'

]);
// trash
Route::get('academic/subject/trash/{id}', [
'as' => 'subject.trash',
'uses' => 'SubjectController@trash',
'title' => 'Trash Subject'


]);
// restore
Route::get('academic/subject/restore/{id}', [
'as' => 'subject.restore',
'uses' => 'SubjectController@restore',
'title' => 'Restore Subject'
]);
// delete
Route::get('academic/subject/delete/{id}', [
'as' => 'subject.delete',
'uses' => 'SubjectController@destroy',
'title' => 'Delete Subject'

]);





//--------------End Of Academic Route-------------------

//---------------Start Professional Training-----------------

/* training category  routes*/
//list
Route::get('training/list', [
    'as' => 'training.index',
    'uses' => 'ProfessionalTrainingController@index',
    'title' => 'Professional Training'
]);
// create
Route::get('training/create', [
    'as' => 'training.create',
    'uses' => 'ProfessionalTrainingController@create',
    'title' => 'Add Professional Training'

]);
// store
Route::post('training/store', [
    'as' => 'training.store',
    'uses' => 'ProfessionalTrainingController@store',
    'title' => 'Store Professional Training'

]);
// edit
Route::get('training/edit/{id}', [
    'as' => 'training.edit',
    'uses' => 'ProfessionalTrainingController@edit',
    'title' => 'Edit Professional Training'
]);
// update
Route::put('training/update/{id}', [
    'as' => 'training.update',
    'uses' => 'ProfessionalTrainingController@update',
    'title' => 'Update Professional Training'

]);
// trash
Route::get('training/trash/{id}', [
    'as' => 'training.trash',
    'uses' => 'ProfessionalTrainingController@trash',
    'title' => 'Trash Professional Training'


]);
// restore
Route::get('training/restore/{id}', [
    'as' => 'training.restore',
    'uses' => 'ProfessionalTrainingController@restore',
    'title' => 'Restore Professional Training'
]);
// delete
Route::get('training/delete/{id}', [
    'as' => 'training.delete',
    'uses' => 'ProfessionalTrainingController@destroy',
    'title' => 'Delete Professional Training'

]);
/* courses  routes*/
//list
Route::get('course/list', [
    'as' => 'course.index',
    'uses' => 'CourseController@index',
    'title' => 'Course'
]);
// create
Route::get('course/create', [
    'as' => 'course.create',
    'uses' => 'CourseController@create',
    'title' => 'Add Course'

]);
// store
Route::post('course/store', [
    'as' => 'course.store',
    'uses' => 'CourseController@store',
    'title' => 'Store Course'

]);
// edit
Route::get('course/edit/{id}', [
    'as' => 'course.edit',
    'uses' => 'CourseController@edit',
    'title' => 'Edit Course'
]);
// update
Route::put('course/update/{id}', [
    'as' => 'course.update',
    'uses' => 'CourseController@update',
    'title' => 'Update Course'

]);
// trash
Route::get('course/trash/{id}', [
    'as' => 'course.trash',
    'uses' => 'CourseController@trash',
    'title' => 'Trash Course'


]);
// restore
Route::get('course/restore/{id}', [
    'as' => 'course.restore',
    'uses' => 'CourseController@restore',
    'title' => 'Restore Course'
]);
// delete
Route::get('course/delete/{id}', [
    'as' => 'course.delete',
    'uses' => 'CourseController@destroy',
    'title' => 'Delete Course'

]);

/* courses modules routes*/
//list
Route::get('module/list/{course_id}', [
    'as' => 'module.index',
    'uses' => 'CourseModulesController@index',
    'title' => 'Course Modules'
]);
// create
Route::get('module/create/{course_id}', [
    'as' => 'module.create',
    'uses' => 'CourseModulesController@create',
    'title' => 'Add Modules'

]);
// store
Route::post('module/store/{course_id}', [
    'as' => 'module.store',
    'uses' => 'CourseModulesController@store',
    'title' => 'Store Modules'

]);
// edit
Route::get('module/edit/{id}', [
    'as' => 'module.edit',
    'uses' => 'CourseModulesController@edit',
    'title' => 'Edit Modules'
]);
// update
Route::put('module/update/{id}', [
    'as' => 'module.update',
    'uses' => 'CourseModulesController@update',
    'title' => 'Update Modules'

]);
// trash
Route::get('module/trash/{id}', [
    'as' => 'module.trash',
    'uses' => 'CourseModulesController@trash',
    'title' => 'Trash Modules'


]);
// restore
Route::get('module/restore/{id}', [
    'as' => 'module.restore',
    'uses' => 'CourseModulesController@restore',
    'title' => 'Restore Modules'
]);
// delete
Route::get('module/delete/{id}', [
    'as' => 'module.delete',
    'uses' => 'CourseModulesController@destroy',
    'title' => 'Delete Modules'

]);
//-----------------------End Of Course Route-----------------