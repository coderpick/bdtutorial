<?php
/**
 * Created by PhpStorm.
 * User: sha1
 * Date: 3/20/17
 * Time: 11:41 AM
 */

function managePagination($obj)
{
    $serial=1;
    if($obj->currentPage()>1)
    {
        $serial=(($obj->currentPage()-1)*$obj->perPage())+1;
    }
    return $serial;
}