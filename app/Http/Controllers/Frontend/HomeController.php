<?php

namespace App\Http\Controllers\Frontend;
use App\Course;
use App\Gallery;
use App\Institution;
use App\Teacher;
use App\InstitutionSlider;
use App\Institutionnews;
use DB;
use App\User;
use App\Historicalplace;
use App\Quciklearn;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
    public function index()
    {
        $data['gallerys']  = Gallery::where('status','active')->paginate(4);
        $data['tutors']    = Teacher::where('status','active')->limit(20)->get();
        $data['courses']   = Course::where('status','active')->where('publish',1)->where('featured',1)->orderBy('sequence','ASC')->limit(4)->get();

        return view('Frontend.index',$data);
    }
    public function gallery()

    {
        $gallerys = Gallery::where('status','active')->paginate(12);
        $divisions = DB::table('divisions')->select('division_id','divisionName')->get();
        return view('Frontend.gallery',compact('gallerys','divisions'));
    }

    public function searchGallery(Request $request)
    {

        $gallerys = Gallery::where('title', 'LIKE', '%'.$request->search.'%' )
            ->orWhere('description','LIKE','%'.$request->search.'%')
            ->get();

        //return $gallerys;

        if ($gallerys){
            foreach ($gallerys as $gallery)
            {
             echo '<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
                <div class="box">
                    <a href="#" data-toggle="modal" data-target="#'.$gallery->id.'">
                        <img src="'.asset($gallery->image).'">
                        <div class="col-md-12 description">
                            <h4>'.$gallery->title.'- Photographer:'.$gallery->author.'</h4>
                        </div>
                    </a>
                    <div class="modal fade" id="'.$gallery->id.'" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 <div class="modal-body">
                                    <img src="'.asset($gallery->image).'">
                                  </div>
                                  <div class="col-md-12 description">
                                    <h4>'.$gallery->description.'</h4>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
            }
        }

    }

    public function searchGalleryByDivision(Request $request)
    {
        $gallerys = Gallery::where('division_id', 'LIKE', '%'.$request->search.'%' )
            ->get();

        if ($gallerys){
            foreach ($gallerys as $gallery)
            {
                echo '<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
                <div class="box">
                    <a href="#" data-toggle="modal" data-target="#'.$gallery->id.'">
                        <img src="'.asset($gallery->image).'">
                        <div class="col-md-12 description">
                            <h4>'.$gallery->title.'- Photographer:'.$gallery->author.'</h4>
                        </div>
                    </a>
                    <div class="modal fade" id="'.$gallery->id.'" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 <div class="modal-body">
                                    <img src="'.asset($gallery->image).'">
                                  </div>
                                  <div class="col-md-12 description">
                                    <h4>'.$gallery->description.'</h4>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
            }
        }
    }


    public function getDistrict(Request $request)
    {
        $districts = DB::table('districts')->select('district_id','districtName')->where('division_id',$request->id)->get();

        // return $districts;
        if($districts){

            foreach ($districts as $district)
            {

                echo '<option value ='.$district->district_id.'>'.$district->districtName.'</option>';
            }
        }
    }

    public function searchGalleryByDistrict(Request $request)
    {
        $gallerys = Gallery::where('district_id',$request->id)->get();

        if ($gallerys){
            foreach ($gallerys as $gallery)
            {
                echo '<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
                <div class="box">
                    <a href="#" data-toggle="modal" data-target="#'.$gallery->id.'">
                        <img src="'.asset($gallery->image).'">
                        <div class="col-md-12 description">
                            <h4>'.$gallery->title.'- Photographer:'.$gallery->author.'</h4>
                        </div>
                    </a>
                    <div class="modal fade" id="'.$gallery->id.'" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 <div class="modal-body">
                                    <img src="'.asset($gallery->image).'">
                                  </div>
                                  <div class="col-md-12 description">
                                    <h4>'.$gallery->description.'</h4>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
            }
        }
    }

    /* sub district */

    public function getSubDistrict(Request $request)
    {
        $subdistricts = DB::table('subdistricts')->select('subdist_id','subdistrictName')->where('district_id',$request->id)->get();

        // return $districts;
        if($subdistricts){
            foreach ($subdistricts as $subdistrict)
            {
                echo '<option value ='.$subdistrict->subdist_id.'>'.$subdistrict->subdistrictName.'</option>';
            }
        }
    }

    public function searchGalleryBysubDistrict(Request $request)
    {
        $gallerys = Gallery::where('subdist_id',$request->id)->get();

        if ($gallerys){
            foreach ($gallerys as $gallery)
            {
                echo '<div class="col-md-4 col-sm-6 co-xs-12 gal-item">
                <div class="box">
                    <a href="#" data-toggle="modal" data-target="#'.$gallery->id.'">
                        <img src="'.asset($gallery->image).'">
                        <div class="col-md-12 description">
                            <h4>'.$gallery->title.'- Photographer:'.$gallery->author.'</h4>
                        </div>
                    </a>
                    <div class="modal fade" id="'.$gallery->id.'" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                 <div class="modal-body">
                                    <img src="'.asset($gallery->image).'">
                                  </div>
                                  <div class="col-md-12 description">
                                    <h4>'.$gallery->description.'</h4>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
            }
        }
    }

    public function showLoginForm()
    {
        return view('Frontend.login');
    }


    public function showRegistrationForm()
    {
        return view('Frontend.registration');
    }
    public function register(Request $request)
    {
        $this->validate($request,[
            'name'          => 'required',
            'email' => 'required|unique:users|max:255',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'image' =>'required'
        ]);
        $user = new User();
        $user->name  = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $image = $request->file('image');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/user/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $user->image = $image_url;
            }
            else
            {

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }
        $user->save();
        Session::flash('message', 'Registration SuccessFully!!');
        return redirect()->back();
    }

    public function QuickLearn()
    {
        $data['quicklearns'] = Quciklearn::where('status', '=','active')->get();
        return view('Frontend.quick_learn',$data);
    }

    public function showresourcePage()
    {
        return  view('Frontend.resource');
    }

    public function historicalplace()
    {
        $data['historicalplaces'] = Historicalplace::where('status','active')->paginate(10);
        return  view('Frontend.historicalplace',$data);
    }
    public function historicalplaceDetails($slug)
    {
        $data['historicalplace'] = Historicalplace::where('slug',$slug)->first();
        return  view('Frontend.historicalplacedetails',$data);
    }

    public function institution()
    {
        return  view('Frontend.institutions');
    }

    public function institutionType($type)
    {
        $institutions       = new Institution();
        $data['institution_list'] = $institutions->where('type',$type)->paginate(20);
        return  view('Frontend.institution_list',$data);
    }
    public function institutiondetails($id,$slug)
    {
        $data['sliders'] = InstitutionSlider::where('institution_id',$id)->paginate(6);
        $data['newses']   = Institutionnews::where('institution_id',$id)->paginate(10);
        $data['institution'] = Institution::where('slug',$slug)->first();
        return  view('Frontend.institution_details',$data);
    }
    public function institutionNewsDetails($id,$slug)
    {
        $data['news'] = Institutionnews::where('slug',$slug)->first();
        $data['institution_news'] = Institutionnews::where('institution_id',$id)->paginate(10);

        return  view('Frontend.institution_news_details',$data);
    }



}
