<?php

namespace App\Http\Controllers\Admin;
use App\Tipscategory;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TipsCategoryController extends Controller
{
    public function index()
    {
        $data['title'] = 'Tips and Ticks Category List';
        $data['categories'] = Tipscategory::orderBy('id','desc')->get();
        return view('quciklearn.categorylist',$data);
    }
    public function create()
    {
        $data['title'] = 'Add Tips and Ticks Category';
        return view('quciklearn.addTipsCategory',$data);
    }
    public function store(Request $request)
    {
        $this->validate($request,[
                'category' => 'required',
                'status'   => 'required'
        ]);
        $category = new  Tipscategory();
        $category->category  = $request->category;
        $category->status    = $request->status;
        $category->save();
        Session::flash('message', 'Category added Successfully!!');
        return redirect()->back();

    }
    public function edit($id)
    {
       $data['title'] = 'Edit Tips and Ticks Category';
       $data['categories'] = Tipscategory::find($id);
       return view('quciklearn.edtiTipsCategory',$data);
    }
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'category' => 'required',
            'status'   => 'required'
        ]);
        $category =Tipscategory::find($id);
        $category->category  = $request->category;
        $category->status    = $request->status;
        $category->save();
        Session::flash('message', 'Category Update Successfully!!');
        return redirect()->back();
    }
    public function delete($id)
    {
        $category =Tipscategory::find($id);
        $category->delete();
        Session::flash('message', 'Category delete Successfully!!');
        return redirect()->back();
    }
}
