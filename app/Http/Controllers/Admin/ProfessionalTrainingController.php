<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\CourseModules;
use App\ProfessionalTraining;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class ProfessionalTrainingController extends Controller
{
    public function index(Request $request){
        $data['title']='Training Category List';
        $category = New ProfessionalTraining();
        if ($request->search == 'trashed') {
            $category = $category->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $category = $category->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $category = $category->where('status', 'active');
        }

        $category = $category->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $category = $category->appends($render);
        }
        $data['categories'] = $category;
        $data['serial'] = managePagination($category);

        return view('professional_training.category.index',$data);
    }
    public function create(){
        $data['title']='Training Category Create';
        return view('professional_training.category.create',$data);
    }
    public function store(Request $request){
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'status' => 'required',
        ]);

        $slug=  str_slug($request->slug);
        $category= New ProfessionalTraining();
        $category->name=$request->name;
        $category->slug=$slug;
        $category->sequence=$request->sequence;
        $category->status=$request->status;
        $category->save();

        Session::flash('message', 'Category Create Successfully.');
        return redirect()->route('training.index');
    }

    public function edit($id)
    {
        $data['title'] = 'Edit Subject';

        $data['category'] = ProfessionalTraining::withTrashed()->where('id', $id)->first();
        $data['slugExist']=Course::where('topic_slug',$data['category']->slug)->count();

        return view('professional_training.category.edit', $data);
    }
    public function update(Request $request,$id){
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);
        $category=ProfessionalTraining::withTrashed()->where('id', $id)->first();
        if(isset($request->newslug)&&$request->newslug!=null){
            $slug=  str_slug($request->newslug);
            $category->slug=$slug;
        }
        $category->name=$request->name;
        $category->sequence=$request->sequence;
        $category->status=$request->status;
        $category->save();

        Session::flash('message', 'Category Update Successfully.');
        return redirect()->route('training.index');
    }
    public function trash($id)
    {
        $category=ProfessionalTraining::findorfail($id);
        $slugExist=Course::where('topic_slug',$category->slug)->count();
        if($slugExist>0){
            Session::flash('warning', 'This Category Can\'t Trashed.');
            return redirect()->back();
        }else{

            $category->delete();
            Session::flash('message', 'Successfully Trashed.');
            return redirect()->back();
        }

    }

    public function restore($id)
    {
        ProfessionalTraining::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully restored.');
        return redirect()->route('training.index');


    }

    public function destroy($id)
    {
        $category=ProfessionalTraining::withTrashed()->where('id', $id)->first();
        $courses= Course::withTrashed()->where('topic_slug',$category->slug)->get();
        foreach($courses as $course){

            if ($course->file!=null) {
                @unlink($course->file);
            }

            CourseModules::withTrashed()->where('course_id',$course->id)->forceDelete();
        }
        Course::withTrashed()->where('topic_slug',$category->slug)->forceDelete();
        $category->forceDelete();

        Session::flash('message', 'Successfully Deleted.');
        return redirect()->route('training.index');

    }
}
