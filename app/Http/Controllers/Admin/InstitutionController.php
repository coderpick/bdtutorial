<?php
namespace App\Http\Controllers\Admin;
use App\Institution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;

class InstitutionController extends Controller
{
    //
    public function index(Request $request)
    {
        $institutions = New Institution();
        $data['title'] = 'Manage Institution';
        if ($request->search == 'trashed') {
            $institutions = $institutions->onlyTrashed();
        } elseif ($request->search == 'inactive') {
            $institutions = $institutions->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $institutions = $institutions->where('status', 'active');
        }

        $institutions = $institutions->orderBy('id', 'DESC')->paginate(Cache::get('per_page'));
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $institutions = $institutions->appends($render);
        }
        $data['institutions'] = $institutions;
        $data['serial'] = managePagination($institutions);
        return view('resource.institution.index', $data);
    }

    public function create()
    {
        $data['title'] = 'Add Institution';
        return view('resource.institution.create',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'institution_type' =>'required',
            'name' =>'required',
            'established' =>'required',
            'overview' =>'required',
            'logo' =>'required',

        ]);

        $institution = new Institution();
        $institution->name                  = $request->name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $institution->slug                  = make_slug($request->name);
        $institution->type                  = $request->institution_type;
        $institution->established           = $request->established;
        $institution->overview              = $request->overview;
        $institution->status                = $request->status;

        $image = $request->file('logo');
        if ($image) {
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/institution/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $institution->logo = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $institution->save();
        Session::flash('message', 'Institution added Successfully!!');
        return redirect()->back();

    }

    public function edit($id)
    {
        $data['title'] = 'Edit Institution';
        $data['institution'] = Institution::withTrashed()->where('id', $id)->first();
        return view('resource.institution.edit',$data);

    }

    public function update(Request $request,$id)
    {
        $this->validate($request,[
            'institution_type' =>'required',
            'name' =>'required',
            'established' =>'required',
            'overview' =>'required',
        ]);

        $institution = Institution::withTrashed()->where('id', $id)->first();
        $institution->name                  = $request->name;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $institution->slug                  = make_slug($request->name);
        $institution->type                  = $request->institution_type;
        $institution->established           = $request->established;
        $institution->overview              = $request->overview;
        $institution->status                = $request->status;

        $image = $request->file('logo');
        if ($image) {
            if($institution->logo !=null)
            {
                @unlink($institution->logo);
            }
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name . '.' . $ext;
            $upload_path = 'upload/resource/institution/';
            $image_url = $upload_path . $image_full_name;
            $image->move($upload_path, $image_full_name);
            if ($ext=='jpg' || $ext=='png'|| $ext=='jpeg'|| $ext=='JPG' || $ext=='PNG'|| $ext=='JPEG'){
                $institution->logo = $image_url;
            }else{

                Session::flash('warning','File is not valid!!');
                return redirect()->back();
            }
        }

        $institution->save();
        Session::flash('message', 'Institution update Successfully!!');
        return redirect()->back();

    }

    public function trash($id)
    {
        Institution::findorfail($id)->delete();
        Session::flash('message', 'Institution successfully Trashed.');
        return redirect()->back();
    }

    public function restore($id)
    {
        Institution::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Institution successfully restored.');
        return redirect()->route('institution.list');

    }
    public function delete($id)
    {
        $institution = Institution::withTrashed()->where('id', $id)->first();
        if ($institution->logo) {
            @unlink($institution->logo);
        }
        $institution->forceDelete();
        Session::flash('message', 'Institution delete Successfully!!');
        return redirect()->route('institution.list');
    }
}
