<?php

namespace App\Http\Controllers\Admin;

use App\Chapter;
use App\ChapterContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
class ChapterController extends Controller
{
    public function index(Request $request,$class,$subject){
        $data['title']='Chapter List'.' / Class- '.ucfirst($class).' / Subject- '.ucfirst($subject);
        $chapter = New Chapter();
        if ($request->search == 'trashed') {
            $chapter = $chapter->onlyTrashed();

        } elseif ($request->search == 'inactive') {
            $chapter = $chapter->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $chapter = $chapter->where('status', 'active');
        }
        $chapter = $chapter->where('class_id', $class);
        $chapter = $chapter->where('subject_id', $subject);
        $chapter = $chapter->orderBy('id', 'DESC')->paginate(10);
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $chapter = $chapter->appends($render);
        }
        $data['chapters'] = $chapter;
        $data['class']=$class;
        $data['subject']=$subject;
        $data['serial'] = managePagination($chapter);

        return view('academic.class._chapter.index',$data);
    }
    public function create($class,$subject){
        $data['title']='New Chapter Create';
        $data['class']=$class;
        $data['subject']=$subject;
        return view('academic.class._chapter.create',$data);
    }
    public function store(Request $request,$class,$subject){
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'status' => 'required',
        ]);
        $slug=  str_slug($request->slug);
        $chapterChek=Chapter::where('class_id',$class)->where('subject_id',$subject)->where('slug',$slug)->count();
        if($chapterChek>0){
            Session::flash('warning', 'Already use this slug for this subject.');
            return redirect()->back()->withInput();
        }else{
            $chapter= New Chapter();
            $chapter->name=$request->name;
            $chapter->slug=$slug;
            $chapter->class_id=$class;
            $chapter->subject_id=$subject;
            $chapter->sequence=$request->sequence;
            $chapter->status=$request->status;
            $chapter->save();
            Session::flash('message', 'Subject Create Successfully.');
            return redirect()->route('subject_chapter.index',[$class,$subject]);
        }


    }
    public function edit($id)
    {
        $data['title'] = 'Edit Chapter';

        $data['chapter'] = Chapter::withTrashed()->where('id', $id)->first();
        $data['slugExist']=ChapterContent::where('class_id',$data['chapter']->class_id)->where('subject_id',$data['chapter']->subject_id)->where('chapter_id',$data['chapter']->slug)->count();
        return view('academic.class._chapter.edit', $data);
    }
    public function update(Request $request,$id){
        $this->validate($request, [
            'name' => 'required',
            'status' => 'required',
        ]);
        $chapter=Chapter::withTrashed()->where('id', $id)->first();
        if(isset($request->newslug)&&$request->newslug!=null){
            $slug=  str_slug($request->newslug);

            $chapterChek=Chapter::where('class_id',$chapter->class_id)->where('subject_id',$chapter->subject_id)->where('slug',$slug)->count();
            if($chapterChek>0){
                Session::flash('warning', 'Already use this slug for this subject.');
                return redirect()->back()->withInput();
            }else{
                $chapter->slug=$slug;
            }


        }

        $chapter->name=$request->name;
        $chapter->sequence=$request->sequence;
        $chapter->status=$request->status;
        $chapter->save();

        Session::flash('message', 'Chapter Update Successfully.');
        return redirect()->route('subject_chapter.index',[$chapter->class_id,$chapter->subject_id]);
    }
    public function trash($id)
    {
        $chapter = Chapter::findorfail($id);
        $slugExist=ChapterContent::where('class_id',$chapter->class_id)->where('subject_id',$chapter->subject_id)->where('chapter_id',$chapter->slug)->count();

        if($slugExist>0){
            Session::flash('warning', 'This Chapter Can\'t Trashed.');
            return redirect()->back();
        }else{

            $chapter->delete();
            Session::flash('message', 'Successfully Trashed.');
            return redirect()->back();
        }

    }
    public function restore($id)
    {
        Chapter::withTrashed()->where('id', $id)->first()->restore();
        Session::flash('message', 'Successfully restored.');
        return redirect()->back();


    }
    public function destroy($id)
    {
        $chapter=Chapter::withTrashed()->where('id', $id)->first();
        ChapterContent::where('class_id',$chapter->class_id)->where('subject_id',$chapter->subject_id)->where('chapter_id',$chapter->slug)->forceDelete();
        $chapter->forceDelete();
        Session::flash('message', 'Successfully Deleted.');
        return redirect()->back();

    }
}
