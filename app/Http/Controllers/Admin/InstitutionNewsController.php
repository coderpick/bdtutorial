<?php

namespace App\Http\Controllers\Admin;
use App\Institutionnews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;

class InstitutionNewsController extends Controller
{
    /* institution news */
    //
    public function index(Request $request,$id)
    {
        $news = New Institutionnews();
        $data['title'] = 'Manage Institution News';
        if ($request->search == 'trashed') {
            $news = $news->onlyTrashed();
        } elseif ($request->search == 'inactive') {
            $news = $news->where('status', 'inactive');
        }elseif ($request->search == 'active') {
            $news = $news->where('status', 'active');
        }

        $news = $news->where('institution_id',$id)->orderBy('id', 'DESC')->paginate(Cache::get('per_page'));
        if (isset($request->search)) {
            $render['search'] = $request->search;
            $news = $news->appends($render);
        }
        $data['news'] = $news;
        $data['id'] = $id;
        $data['serial'] = managePagination($news);
        return view('resource.institution.news.news_index', $data);
    }

    public function create($id)
    {
        $data['title'] = 'Add Institution News';
        $data['title'] ="News add ";
        $data['id']    = $id;
        return view('resource.institution.news.create_news',$data);
    }

    public function store(Request $request,$id)
    {

        $this->validate($request,[
            'title' =>'required',
            'details' =>'required',
        ]);

        $institution_news = new Institutionnews();
        $institution_news->title         = $request->title;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $institution_news->slug           = make_slug($request->title);
        $institution_news->details         = $request->details;
        $institution_news->status         = $request->status;
        $institution_news->institution_id = $request->id;

        $institution_news->save();
        Session::flash('message', 'Institution news added Successfully!!');
        return redirect()->route('institution.news.list',$institution_news->institution_id);

    }

    public function edit($id)
    {
        $data['title'] = 'Edit Institution News';
        $data['news'] = Institutionnews::withTrashed()->where('id', $id)->first();
        return view('resource.institution.news.edit_news',$data);

    }

    public function update(Request $request,$id)
    {
        $institution_news = Institutionnews::withTrashed()->where('id', $id)->first();

        $institution_news->title         = $request->title;
        function make_slug($string) {
            $slug = preg_replace('/\s+/u', '-', trim($string));
            $slug = mb_strtolower($slug);
            return $slug;
        }
        $institution_news->slug           = make_slug($request->title);
        $institution_news->details         = $request->details;
        $institution_news->status         = $request->status;
        $institution_news->save();
        Session::flash('message', 'Institution news update Successfully!!');
        return redirect()->route('institution.news.list',$institution_news->institution_id);
    }

    public function trash($id)
    {
        Institutionnews::findorfail($id)->delete();
        Session::flash('message', 'Institution news successfully Trashed.');
        return redirect()->back();
    }

    public function restore($id)
    {
        $institute_news = Institutionnews::withTrashed()->where('id', $id)->first();
        $institute_news->restore();
        Session::flash('message', 'Institution news successfully restored.');
        return redirect()->route('institution.news.list',$institute_news->institution_id);

    }
    public function delete($id)
    {
        $news = Institutionnews::withTrashed()->where('id', $id)->first();
        $news->forceDelete();
        Session::flash('message', 'Institution news delete Successfully!!');
        return redirect()->route('institution.news.list',$news->institution_id);
    }
}
