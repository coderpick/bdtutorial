<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class Chapter extends Model
{
    use SoftDeletes;

    protected $table='chapters';
    protected $fillable = [
        'name', 'slug', 'sequence','subject_id','class_id','status'
    ];
    protected $dates = ['deleted_at'];

    // Relation Content Table
    public function relContent()
    {
        return $this->hasMany('App\ChapterContent', 'chapter_id', 'id');
    }

    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
