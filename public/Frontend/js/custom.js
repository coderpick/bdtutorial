
/*scroll fix nav*/
window.onscroll = function(){
    var menu = document.getElementsByClassName("menu"),
        heading = document.getElementById('page_heading'),
        banner = document.getElementById("banner");
    if (document.body.scrollTop > 111 || document.documentElement.scrollTop > 111){
        menu[0].style.position = "fixed";
        menu[0].style.top = 0;
        menu[0].style.left = 0;
        menu[0].style.width = "100%";
        if(banner!==null){  banner.style.marginTop = '50px';}
        if(heading!==null){  heading.style.marginTop = '50px';}
    }else{
        menu[0].style.position = "static";
        if(banner!==null){  banner.style.marginTop = '0px';}
        if(heading!==null){  heading.style.marginTop = '0px';}
    }
}
/*scroll fix nav*/
/*show dropdown on hover*/
    $(document).ready(function(){
        $(".dropdown").hover(
            function() {
                $('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideDown("fast");
                $(this).toggleClass('open');
            },
            function() {
                $('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideUp("fast");
                $(this).toggleClass('open');
            }
        );
    });
/*show dropdown on hover*/
/*tutor slide*/
$(document).ready(function(){
    $('.teacher_slide').slick({
        dots: true,
        infinite: true,
        autoplay:false,
        autoplaySpeed:3000,
        speed: 600,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
        ]
    });
});



/*tutor slide*/
/*total rating*/
$(document).ready(function() {
    $('.bar span').hide();
    $('#bar-five').animate({
        width: '75%'}, 1000);
    $('#bar-four').animate({
        width: '35%'}, 1000);
    $('#bar-three').animate({
        width: '20%'}, 1000);
    $('#bar-two').animate({
        width: '15%'}, 1000);
    $('#bar-one').animate({
        width: '30%'}, 1000);

    setTimeout(function() {
        $('.bar span').fadeIn('slow');
    }, 1000);
});
/*total rating*/

/*give rating*/
$('.ratingg input').each(function () {
    var $radio = $(this);
    $('.ratingg .selected').removeClass('selected');
    $radio.closest('label').addClass('selected');

    // $('.ratingg input').each(function () {
    //     var rate = $(this).val();
    //     console.log(rate);
    // })
});


/*give rating*/

/*review slide*/
$(document).ready(function(){
    $('.review_slide').slick({
        dots: true,
        autoplay:true,
        autoplaySpeed:3000,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        adaptiveHeight: true
    });
});
/*review slide*/
/*course comments*/
$('#comments-container').comments({
    profilePictureURL: 'https://app.viima.com/static/media/user_profiles/user-icon.png',
    getComments: function(success, error) {
        var commentsArray = [{
            id: 1,
            created: '2017-07-12',
            content: 'Lorem ipsum dolort sit amet',
            fullname: 'Tauhid',
            profile_picture_url: 'images/comments.jpg',
            upvote_count: 2,
            user_has_upvoted: false
        }];
        success(commentsArray);
    }
});

/*course comments*/
/*modal*/
$(function(){
    if(!sessionStorage.getItem("modalFirst")){
        // show the modal onload
        $('#modal-content').modal({
            show: true
        });
    }
    $('#modal-content').on('hide.bs.modal', function (e) {
        sessionStorage.setItem("modalFirst","1");
    })
});
function destroySession() {
    sessionStorage.removeItem('modalFirst');
}
/*modal*/
/*school banner slide*/
$(document).ready(function(){
    $('.school_img_slide').slick({
        dots: true,
        autoplay:true,
        autoplaySpeed:3000,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        adaptiveHeight: true
    });
});
/*school banner slide*/
/*====================Onclick slide nav=======================*/
$('#fp-nav a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
        event.preventDefault();
        $('html, body').stop().animate({
            scrollTop: target.offset().top
        }, 1000);
    }
});
/*====================Onclick slide nav=======================*/