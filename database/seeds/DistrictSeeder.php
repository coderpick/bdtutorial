<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class DistrictSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('districts')->insert([
            [ 'district_id'=>1, 'division_id'=> '1', 'districtName'=> ' Dhaka City-ঢাকা সিটি', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>2, 'division_id'=> '1', 'districtName'=> ' Faridpur-ফরিদপুর', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>3, 'division_id'=> '1', 'districtName'=> ' Gazipur-গাজিপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>4, 'division_id'=> '1', 'districtName'=> ' Gopalganj-গোপারগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>5, 'division_id'=> '1', 'districtName'=> ' Jamalpur-জামালপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>6, 'division_id'=> '1', 'districtName'=> ' Kishoreganj-কিশোরগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>7, 'division_id'=> '1', 'districtName'=> ' Madaripur-মাদারিপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>8, 'division_id'=> '1', 'districtName'=> ' Manikganj-মানিকগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>9, 'division_id'=> '1', 'districtName'=> ' Munshiganj-মুন্সিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>10, 'division_id'=> '1', 'districtName'=> ' Mymensingh-ময়মনসিংহ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>11, 'division_id'=> '1', 'districtName'=> ' Narayanganj-নারায়নগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>12, 'division_id'=> '1', 'districtName'=> ' Narsingdi-নরসিংদি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>13, 'division_id'=> '1', 'districtName'=> ' Netrakona-নেত্রোকোনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>14, 'division_id'=> '1', 'districtName'=> ' Rajbari-রাজবাড়ি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>15, 'division_id'=> '1', 'districtName'=> ' Shariatpur-শরিয়তপুর', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>16, 'division_id'=> '1', 'districtName'=> ' Sherpur-শেরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>17, 'division_id'=> '1', 'districtName'=> ' Tangail-টাঙ্গাইল', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>18, 'division_id'=> '1', 'districtName'=> ' Dhamrai-ধামরাই ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>19, 'division_id'=> '1', 'districtName'=> ' Dohar-দোহার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>20, 'division_id'=> '1', 'districtName'=> ' Keraniganj-কেরানীগঞ্জ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>21, 'division_id'=> '1', 'districtName'=> ' Nawabganj-নওয়াবগঞ্জ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>22, 'division_id'=> '1', 'districtName'=> ' Savar-সাভার', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>23, 'division_id'=> '2', 'districtName'=> ' Barisal City-বরিশাল সিটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>24, 'division_id'=> '2', 'districtName'=> ' Barguna-বরগুনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>25, 'division_id'=> '2', 'districtName'=> ' Bhola-ভোলা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>26, 'division_id'=> '2', 'districtName'=> ' Jhalokati-ঝালকাঠি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>27, 'division_id'=> '2', 'districtName'=> ' Patuakhali-পটুয়াখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>28, 'division_id'=> '2', 'districtName'=> ' Pirojpur-পিরোজপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>29, 'division_id'=> '3', 'districtName'=> ' Chittagong City-চট্টগ্রাম সিটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>30, 'division_id'=> '3', 'districtName'=> ' Bandarban-বান্দরবন ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>31, 'division_id'=> '3', 'districtName'=> ' Brahmanbaria-ব্রামনবাড়ীয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>32, 'division_id'=> '3', 'districtName'=> ' Chandpur-চাঁদপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>33, 'division_id'=> '3', 'districtName'=> ' Comilla-কুমিল্লা্ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>34, 'division_id'=> '3', 'districtName'=> ' Coxs Bazar-কক্সবাজার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>35, 'division_id'=> '3', 'districtName'=> ' Feni-ফেনী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>36, 'division_id'=> '3', 'districtName'=> ' Khagrachhari-খাগরাছরী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>37, 'division_id'=> '3', 'districtName'=> ' Lakshmipur-লক্ষীপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>38, 'division_id'=> '3', 'districtName'=> ' Noakhali-নোয়াখালী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>39, 'division_id'=> '3', 'districtName'=> ' Rangamati-রাঙ্গামাটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>40, 'division_id'=> '4', 'districtName'=> ' Rajshahi City-রাজশাহী সিটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>41, 'division_id'=> '4', 'districtName'=> ' Bogra-বগুড়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>42, 'division_id'=> '4', 'districtName'=> ' Joypurhat-জয়পুরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>43, 'division_id'=> '4', 'districtName'=> ' Naogaon-নওগা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>44, 'division_id'=> '4', 'districtName'=> ' Natore-নাটোর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>45, 'division_id'=> '4', 'districtName'=> ' Nawabganj-নওয়াবগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>46, 'division_id'=> '4', 'districtName'=> ' Pabna-পাবনা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>47, 'division_id'=> '4', 'districtName'=> ' Sirajganj-সিরাজগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>48, 'division_id'=> '5', 'districtName'=> ' Sylhet City-সিলেট সিটি', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>49, 'division_id'=> '5', 'districtName'=> ' Habiganj-হবিগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>50, 'division_id'=> '5', 'districtName'=> ' Moulvibazar-মওলবিবাজার ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>51, 'division_id'=> '5', 'districtName'=> ' Sunamganj-সুনামগঞ্জ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>52, 'division_id'=> '6', 'districtName'=> ' Rangpur City-রংপুরসিটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>53, 'division_id'=> '6', 'districtName'=> ' Dinajpur-দিনাজপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>54, 'division_id'=> '6', 'districtName'=> ' Gaibandha-গাইবান্ধা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>55, 'division_id'=> '6', 'districtName'=> ' Kurigram-কুরিগ্রমি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>56, 'division_id'=> '6', 'districtName'=> ' Lalmonirhat-লালমনিরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>57, 'division_id'=> '6', 'districtName'=> ' Nilphamari-নীলফামারী ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>58, 'division_id'=> '6', 'districtName'=> ' Panchagarh-পঞ্চগড় ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>59, 'division_id'=> '6', 'districtName'=> ' Thakurgaon-ঠাকুরগাঁ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>60, 'division_id'=> '7', 'districtName'=> ' Khulna City-খুলনা সিটি ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>61, 'division_id'=> '7', 'districtName'=> ' Bagerhat-বাগেরহাট ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>62, 'division_id'=> '7', 'districtName'=> ' Chuadanga-চুয়াড়াঙ্গা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>63, 'division_id'=> '7', 'districtName'=> ' Jessore-জশোর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>64, 'division_id'=> '7', 'districtName'=> ' Jhenaidah-ঝিনাইদহ ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>65, 'division_id'=> '7', 'districtName'=> ' Kushtia-কুষ্ঠিয়া ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>66, 'division_id'=> '7', 'districtName'=> ' Magura-মাগুরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>67, 'division_id'=> '7', 'districtName'=> ' Meherpur-মেহেরপুর ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>68, 'division_id'=> '7', 'districtName'=> ' Narail-নরাইল ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
            [ 'district_id'=>69, 'division_id'=> '7', 'districtName'=> ' Satkhira-সাতকিরা ', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s') ],
        ]);
        $this->command->info("districts table seeded :) created by Rejvi email:rejvi.nomani@gmail.com");
    }
}
