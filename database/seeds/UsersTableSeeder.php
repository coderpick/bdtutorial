<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([


            [
                'id'=>1,
                'name'=> 'Admin',
                'email'=> 'admin@piit.us',
                'status'=> 'Active',
                'password'=> bcrypt("admin@123"),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],

            [
                'id'=>2,
                'name'=> 'Rejvi Nomani',
                'email'=> 'nomani@piit.us',
                'password'=> bcrypt("admin@123"),
                'status'=> 'Active',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],

            [
                'id'=>3,
                'name'=>'Shawon',
                'email'=> 'shawon.piit@gmail.com',
                'password'=> bcrypt("admin@123"),
                'status'=> 'Active',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'id'=>4,
                'name'=>'Farhan Monsi',
                'email'=> 'info@farhanmonsi.com',
                'password'=> bcrypt("admin@123"),
                'status'=> 'Active',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]


    ]);
    }
}
