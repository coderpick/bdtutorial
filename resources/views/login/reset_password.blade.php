<!DOCTYPE html>
<html>
<head>
    @include('layouts._head')
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Reset Your Password</p>
        @include('layouts._message')
        {!! Form::model($user,['route'=>['update_password',$user->id],'method'=>'put']) !!}
        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::password('password', ['class'=>'form-control','id'=>'Password','placeholder'=>'password','required','minlength'=>'6']) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
            {{ Form::password('password_confirmation', ['class'=>'form-control','id'=>'Password','placeholder'=>'Confirm password','required','minlength'=>'6']) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password_confirmation'))
                <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
            @endif
        </div>
        <div class="row">

            <div class="col-xs-4">
                {!! Form::submit('Reset',['class'=>'btn btn-primary btn-block btn-flat']) !!}

            </div>
            <!-- /.col -->
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.login-box-body -->
</div>
@include('layouts._script')
</body>
</html>