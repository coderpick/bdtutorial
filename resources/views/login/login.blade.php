<!DOCTYPE html>
<html>
<head>
    @include('layouts._head')
    <link rel="stylesheet" href="{!! asset('asset/plugins/iCheck/square/blue.css') !!}">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <span class="logo-lg"><b>BD Tutorial </b>Zone</span>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        @include('layouts._message')
        {!! Form::open(['route'=>'login_check','method'=>'post']) !!}

        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            {{ Form::text('email',null,['class'=>'form-control','id'=>'email_address','placeholder'=>'Email','required']) }}
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            {{ Form::password('password', ['class'=>'form-control','id'=>'Password','placeholder'=>'Password','required','minlength'=>'6']) }}
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck">
                    <label style="margin: 10px;">
                        <input type="checkbox" name="remember_me"> Remember Me
                    </label>
                </div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                {!! Form::submit('Sign In',['class'=>'btn btn-primary btn-block btn-flat']) !!}

            </div>
            <!-- /.col -->
        </div>
    {!! Form::close() !!}
        <a href="#" data-toggle="modal" data-target="#myModal">Forgot your Password?</a>
{{--        <br/><a href="{!! route('regUser') !!}" class="text-center">Register a new membership</a>--}}

    </div>
    <div class="pull-right" style=" padding-top: 10px;">
        <b>Powered By :</b><a href="http://peoplentech.net" target="_blank" title="PeopleNTech Software"><strong>
                PeopleNTech Software </strong></a>
    </div>
    <!-- /.login-box-body -->
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Enter Your Email</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>'reset_user_password','role'=>'form']) !!}

                <div class="form-group">
                    {{ Form::text('email',null,['class'=>'form-control','placeholder'=>'Enter Your Email','required']) }}
                    @if ($errors->has('email'))
                        <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                    @endif
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
                {!!Form::close()!!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@include('layouts._script')
    <script src="{!! asset('asset/plugins/iCheck/icheck.min.js') !!}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>

</body>
</html>