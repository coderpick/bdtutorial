<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(Auth::user()->image!=null)
                    <img src="{{asset(Auth::user()->image)}}" class="img-circle" alt="User Image">
                @endif

                @if(Auth::user()->image==null)
                    <img src="{{asset('asset/img/user_logo.jpg')}}" class="user-image" alt="User Image">
                @endif
            </div>
            <div class="pull-left info">
                <p>{!! Auth::user()->name !!}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Dashboard</li>
            <!-- Optionally, you can add icons to the links -->

            <li class="treeview">
                <a href="{!! route('dashboard') !!}"><i class="fa fa-user"></i> <span>Users</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('user.index') !!}"><i class="fa fa-angle-double-right"></i>Users List</a></li>
                    <li><a href="{!! route('addUser') !!}"><i class="fa fa-angle-double-right"></i>Add User</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="{!! route('gallery.index') !!}"><i class="fa fa-image"></i> <span>Gallery</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('gallery.index') !!}"><i class="fa fa-angle-double-right"></i>Gallery List</a></li>
                    <li><a href="{!! route('gallery.add') !!}"><i class="fa fa-angle-double-right"></i>Add Gallery</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="{!! route('gallery.index') !!}"><i class="fa fa-users"></i> <span>Tutors</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('teachers') !!}"><i class="fa fa-angle-double-right"></i>Tutor List</a></li>
                    <li><a href="{!! route('teacher.add') !!}"><i class="fa fa-angle-double-right"></i>Add Tutor</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{!! route('gallery.index') !!}"><i class="fa fa-book"></i> <span>Quick Learn</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('quick.learn.list') !!}"><i class="fa fa-angle-double-right"></i>Tips&Ticks List</a></li>
                    <li><a href="{!! route('quick.learn.add') !!}"><i class="fa fa-angle-double-right"></i>Tips&Ticks Add</a></li>
                    <li><a href="{!! route('tips.category.list') !!}"><i class="fa fa-angle-double-right"></i>Tips&Ticks Category</a></li>

                </ul>
            </li>
            <li class="treeview">
                <a href="{!! route('gallery.index') !!}"><i class="fa fa-chrome"></i> <span>Resources</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('historical.add') !!}"><i class="fa fa-angle-double-right"></i>Add Historical Place</a></li>
                    <li><a href="{!! route('historical.list') !!}"><i class="fa fa-angle-double-right"></i>Historical Place</a></li>
                    <li><a href="{!! route('institution.list') !!}"><i class="fa fa-angle-double-right"></i>Institution List</a></li>
                    <li><a href="{!! route('institution.add') !!}"><i class="fa fa-angle-double-right"></i>Add Institution</a></li>
                    <li><a href="{!! route('book.category.list') !!}"><i class="fa fa-angle-double-right"></i>E-book Category</a></li>
                    <li><a href="{!! route('book.list') !!}"><i class="fa fa-angle-double-right"></i>E-books</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-life-saver"></i> <span>Academic</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('class.index') !!}"><i class="fa fa-angle-double-right"></i>Class List</a></li>
                    <li><a href="{!! route('subject.index') !!}"><i class="fa fa-angle-double-right"></i>Subject List</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#"><i class="fa fa-life-saver"></i> <span>Professional Training</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('training.index') !!}"><i class="fa fa-angle-double-right"></i>Training Category List</a></li>
                    <li><a href="{!! route('course.index') !!}"><i class="fa fa-angle-double-right"></i>Course List</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#"><i class="fa  fa-cog"></i> <span>Sittings</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{!! route('block_ip.index') !!}"><i class="fa fa-angle-double-right"></i>Blocked Ip</a></li>

                </ul>
            </li>

        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>