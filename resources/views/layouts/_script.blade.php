@yield('js')

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 2.2.3 -->

<!-- AdminLTE App -->
<script src="{!! asset('asset/dist/js/app.min.js')!!}"></script>
@yield('customJs')