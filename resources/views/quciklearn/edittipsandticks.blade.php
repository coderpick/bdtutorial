@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <form action="{{ route('quick.learn.update',$quicklearns->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts._message')
            <div class="box-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Tips Title</label>
                            <input type="text" name="title" id="inputName" class="form-control" value="{!! $quicklearns->title !!}">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Video URL</label>
                            <input type="url" name="url" id="inputEmail" class="form-control" value="{!! $quicklearns->url !!}">
                        </div>
                        <div class="form-group">
                            <label for="typeDescription">Tips Descriptions</label>
                            <textarea name="description" id="typeDescription" class="form-control">{!! $quicklearns->description !!}</textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="Categoryinut">Select Category</label>
                            <select name="category" class="form-control" id="Categoryinut">
                                <option selected disabled>Choose Category</option>
                                @forelse($categories as $category)
                                    <option @if( $quicklearns->cat_id == $category->id) selected @endif value="{!! $category->id !!}">{!! $category->category !!}</option>
                                @empty
                                    <p>No record found!</p>
                                @endforelse
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status"  {!! $quicklearns->status == 'active' ? 'checked' : ''  !!}  value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status"  {!! $quicklearns->status == 'inactive' ? 'checked' : ''  !!}  value="inactive"> Inactive</label>
                            <label class="radio-inline"><input type="radio" name="status"  {!! $quicklearns->status == 'suspended' ? 'checked' : ''  !!}  value="suspended"> Suspended</label>
                        </div>
                        <div class="form-group">
                            <img style="height: 166px;width:258px;" src="{!! asset($quicklearns->feature_image) !!}" id="preview2">
                            <div class="upload-box">
                                <label class="btn" style="background: gainsboro;width: 50%;">
                                    <input name="featureimage"  onchange="document.getElementById('preview2').src = window.URL.createObjectURL(this.files[0])" style="display:none" type="file">
                                    <i class="fa fa-cloud-upload"></i> Upload Feature Image
                                    <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                                       style="color:green;margin-top:10px;display: none"></i>
                                </label>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <input type="submit" name="update"  value="Update" class="btn btn-success pull-right">
                    </div>
                    <div class="col-xs-6">
                        <a href="{!! route('quick.learn.list') !!}" class="btn btn-warning" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('select2js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bl182cv0ud67yxrbs9uor8xfscrz38s7bb5swlqoo5y4v7o6"></script>
    <script src="{!! asset('dist/js/select2.min.js') !!}"></script>
    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 160,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css']
            });
        })
    </script>
@endsection
@section('customJs')
    <script type="text/javascript">
/*
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload1").change(function(){
            readURL(this);
        });
        $("#imgupload2").change(function(){
            readURL(this);
        });
*/

    </script>
@endsection