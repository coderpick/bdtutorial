@extends('layouts.master')

@section('content')

    <div class="box box-default">

        {!! Form::model($category,['route'=>['training.update',$category->id],'method'=>'put','files'=> true]) !!}
        @include('layouts._message')
        <div class="box-body">
            <div class="row">
                @include('professional_training.category._form')
            </div>
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::submit('Update',['class'=>'btn btn-success pull-right']) !!}

                </div>
                <div class="col-xs-6">
                    <a href="{!! route('training.index') !!}" class="btn btn-danger"
                       onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection
