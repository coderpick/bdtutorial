<div class="col-md-6 col-md-offset-3">

    <div class="form-group">
        {!! Form::label('Name') !!}
        :
        {!! Form::text('name',null,['class'=>'form-control']) !!}
    </div>


    <div class="form-group">
        {!! Form::label('Slug') !!}
        : <i style="color:red"><b>( Slug should be english always )</b></i>
        @if(!isset($category))
        {!! Form::text('slug',null,['class'=>'form-control']) !!}
        @else
            @if($slugExist>0)
            {!! Form::text('slug',null,['class'=>'form-control','readonly']) !!}
            @else
                {!! Form::text('slug',null,['class'=>'form-control','readonly']) !!}
            {!! Form::text('newslug',null,['class'=>'form-control','placeholder'=>'Update New Slug']) !!}
            @endif
        @endif
    </div>
    <div class="form-group">
        {!! Form::label('Sequence') !!}
        :
        {!! Form::number('sequence',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Status') !!}
        :
        {!! Form::radio('status','active',null,['class'=>'minimal','checked']) !!} Active
        {!! Form::radio('status','inactive',null,['class'=>'minimal']) !!} Inactive
    </div>

</div>
