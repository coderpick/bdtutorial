@extends('layouts.master')
@section('content')

    <div class="box box-default">

        {!! Form::open(['route'=>['module.store',$course_id],'method'=>'post','files'=> true]) !!}

        @include('layouts._message')
        <div class="box-body">
            <div class="row">
                @include('professional_training.module._form')
            </div>
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::submit('Save',['class'=>'btn btn-success pull-right']) !!}

                </div>
                <div class="col-xs-6">
                    <a href="{!! route('module.index',$course_id) !!}" class="btn btn-danger" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection
