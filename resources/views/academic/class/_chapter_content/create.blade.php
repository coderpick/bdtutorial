@extends('layouts.master')
@section('content')

    <div class="box box-default">

        {!! Form::open(['route'=>['chapter_content.store',$class,$subject,$chapter],'method'=>'post','files'=> true]) !!}

        @include('layouts._message')
        <div class="box-body">
            <div class="row">
                @include('academic.class._chapter_content._form')
            </div>
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::submit('Save',['class'=>'btn btn-success pull-right']) !!}

                </div>
                <div class="col-xs-6">
                    <a href="{!! route('chapter_content.index',[$class,$subject,$chapter]) !!}" class="btn btn-danger" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection
