@extends('layouts.master')

@section('content')

    <div class="box box-default">

        {!! Form::model($subject,['route'=>['class_subject.update',$subject->id],'method'=>'put','files'=> true]) !!}
        @include('layouts._message')
        <div class="box-body">
            <div class="row">
                @include('academic.class._class_subject._form')
            </div>
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::submit('Update',['class'=>'btn btn-success pull-right']) !!}

                </div>
                <div class="col-xs-6">
                    <a href="{!! route('class_subject.index',$subject->slug) !!}" class="btn btn-danger"
                       onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <!-- /.box -->
@endsection
