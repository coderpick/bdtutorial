<div class="col-md-6 col-md-offset-3">

    @if(isset($subjectList))
    <div class="form-group">
        {!! Form::label('Subject') !!}
        {!! Form::select('subject',$subjectList,null,['class'=>'form-control','placeholder'=>'-- Please Select Subject --','required']) !!}
    </div>
    @endif
    <div class="form-group">
        {!! Form::label('Subject Overview Video Link ( Embed Code Url Only)') !!}
        :
        {!! Form::url('url',null,['class'=>'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('Status') !!}
        :
        {!! Form::radio('status','active',null,['class'=>'minimal','checked']) !!} Active
        {!! Form::radio('status','inactive',null,['class'=>'minimal']) !!} Inactive
    </div>

</div>
