@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')
    <div class="box box-default">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <form action="{{ route('book.category.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts._message')
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="form-group">
                            <label for="inputName">Category Name</label>
                            <input type="text" name="name" id="inputName" class="form-control" placeholder="Enter book category name">
                        </div>
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status" checked value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status" value="inactive"> Inactive</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <input type="submit" name="Save"  value="Submit" class="btn btn-success pull-right">
                    </div>
                    <div class="col-xs-6">
                        <a href="{!! route('book.category.list') !!}" class="btn btn-warning" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('select2js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bl182cv0ud67yxrbs9uor8xfscrz38s7bb5swlqoo5y4v7o6"></script>
    <script src="{!! asset('dist/js/select2.min.js') !!}"></script>
    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 200,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css']
            });
        })
    </script>
@endsection
@section('customJs')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload").change(function(){
            readURL(this);
        });

    </script>
@endsection