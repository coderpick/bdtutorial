@extends('Frontend.layouts.master')

@section('content')
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                </div>
                <div class="page_heading">
                    <h1>Quick Learn</h1>
                </div>
            </div>
        </div>
    </section>
<!--======##########=========page content=========##########======-->
<section id="quick_learn">
    <div class="container pdnt-30 pdnb-50">
        <div class="row pdnt-30">
            @forelse($quicklearns as $quicklearn)
            <div class="col-md-3" style="margin-bottom: 15px;">
                <div class="resource_img text-center">
                    <img src="{!! asset($quicklearn->feature_image) !!}" alt="">
                </div>
                <div class="resource_img_hover">
                    <a href="{!! route('quicklearn.details',$quicklearn->slug) !!}">{!! $quicklearn->title !!}</a>
                </div>
            </div>
            @empty
             <p>No record found</p>
            @endforelse
        </div>

    </div>
</section>
<!--======##########=========page content=========##########======-->
@endsection
