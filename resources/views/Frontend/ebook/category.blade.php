@extends('Frontend.layouts.master')

    @section('content')
        <section id="page_heading">
            <div class="container-fluid">
                <div class="row">
                    <div class="page_heading_back">
                        <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                    </div>
                    <div class="page_heading">
                        <h1>eBooks Collection</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container pdnt-30 pdnb-50">
            <div class="row pdnt-30">
                @forelse($books as $book)
                <div class="col-sm-6 col-md-3 col-lg-3 margin-bottom-10">
                    <div class="single-book text-center">
                            <img class="book-img" src="{!! asset($book->book_cover) !!}" alt="">
                            <h5>{!! $book->name !!} </h5>
                            <h6>  Author:{!! $book->author !!} </h6>
                        <div class="book-hover">
                            <a  href="{!! route('ebook.info',$book->slug) !!}">View</a>
                        </div>
                    </div>
                </div>
                @empty
                <p>No book available!</p>
                @endforelse
            </div>


            </div>
        </div>
    @endsection

