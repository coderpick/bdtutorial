@extends('Frontend.layouts.master')

    @section('content')
        <section id="page_heading">
            <div class="container-fluid">
                <div class="row">
                    <div class="page_heading_back">
                        <img src="{!! asset('Frontend/images/page_heading.jpg') !!}" alt="">
                    </div>
                    <div class="page_heading">
                        <h1>eBooks Collection</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container pdnt-30 pdnb-50">
            <div class="row pdnt-30">
                @forelse($categories as $category)
                <div class="col-sm-6 col-md-3 col-lg-3 margin-bottom-10">
                    <a href="{!! route('ebook.category',[$category->id,$category->slug]) !!}">
                        <div class="booksubject text-center">
                            <img src="{!! asset('Frontend/images/book.png') !!}" alt="">
                            <div class="booksubjectCaption text-center">
                                <h5>{!! $category->category_name !!}</h5>
                            </div>
                        </div>
                    </a>
                </div>
                @empty
                <p>No books available</p>
                @endforelse

            </div>
        </div>
    @endsection

