@extends('Frontend.layouts.master')

@section('content')
    <!--======##########=========page heading=========##########======-->
    <section id="page_heading">
        <div class="container-fluid">
            <div class="row">
                <div class="page_heading_back">
                    <img src="{!! asset('Frontend/images/school_baner.jpg') !!}" alt="">
                </div>
                <div class="page_heading" style="background:#f9edc7;border: 0;">
                    <h1 style="color:#fff;text-shadow: 2px 2px 2px #222;">{!! $institution->name !!}</h1>
                    <p class="established">Established in {!! $institution->established !!}</p>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page heading=========##########======-->

    <!--======##########=========page content=========##########======-->
    <section id="school_details">
        <div class="container pdnt-30">
            <div class="row">
                <div class="col-md-8">
                    <div class="school_img_slide" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}'>
                        @forelse($sliders as $slider )
                        <div class="school_img"><img src="{!! asset($slider->image) !!}" alt="school image"></div>
                        @empty
                            <div class="school_img"><img src="{!! asset('Frontend/images/default.jpg') !!}" alt="school image"></div>
                            <div class="school_img"><img src="{!! asset('Frontend/images/default.jpg') !!}" alt="school image"></div>
                         @endforelse
                    </div>
                    <div class="pdnt-20">
                        <span class="title pdnt-10">Overview </span>
                        <hr>
                        <p class="page_text" style="color:#1e3d4f;">
                            {!! $institution->overview !!}
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="school_panel">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title title">Institution News</h3>
                            </div>
                            <ul class="list-group">
                                @forelse($newses as $news)
                                    <a href="{!! route('institution.news.details',[$news->institution_id,$news->slug]) !!}" class="list-group-item">{!! $news->title !!}</a>
                                @empty
                                    <p>News not available</p>
                                @endforelse

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--======##########=========page content=========##########======-->
@endsection
