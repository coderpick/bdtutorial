@extends('layouts.master')
@section ('content')

    <div class="row">
        <div class="col-xs-12">
            <!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <div class="col-sm-12" style="padding-top: 5px;padding-bottom: 5px;">
                        <a href="{!! route('teachers') !!}" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Back</a>
                        <a href="{{route('add.skill',$id)}}" class="btn btn-success pull-right addNew">Add Skill</a>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive">
                    <table  class="table table-bordered table-striped">
                        @include('layouts._message')
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Skill</th>
                            <th class="text-center" style="width: 10%;">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($teacherskills as $teacherskill)
                                <tr>
                                    <td>{!! $teacherskill->title !!}</td>
                                    <td>{!! $teacherskill->skilllimit !!}</td>
                                    <td>
                                        <a class="btn btn-info btn-sm" href="{!! route('teacher.skill.edit',$teacherskill->id) !!}"><i class="fa fa-edit"></i></a>
                                        <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to delete?')" href="{!! route('teacher.skill.delete',$teacherskill->id) !!}"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="pull-right">

            </div>

            <!-- /.box -->
        </div>
    </div>

@endsection