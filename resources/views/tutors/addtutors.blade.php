@extends('layouts.master')
@section('select2css')
    <link rel="stylesheet" href="{!! asset('dist/css/select2.min.css') !!}">
@endsection
@section('content')

    <div class="box box-default">
        <div class="box-header with-border">
            <!-- /.box-header -->

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                    <i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <form action="{{ route('teacher.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('layouts._message')
            <div class="box-body">
                <div class="row">

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="inputName">Name</label>
                            <input type="text" name="name" id="inputName" class="form-control" placeholder="Enter Name">
                        </div>
                        <div class="form-group">
                            <label for="inputTitle">Profession</label>
                            <input type="text" name="title" id="inputTitle" class="form-control" placeholder="ex. web developer or graphic designer">
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email</label>
                            <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Enter email">
                        </div>
                        <div class="form-group">
                            <label for="inputMobile">Mobile No.</label>
                            <input type="text" name="mobile" id="inputMobile" class="form-control" placeholder="Enter mobile number">
                        </div>
                        <div class="form-group">
                            <label for="inputURL">URL</label>
                            <input type="url" name="url" id="inputURL" class="form-control" placeholder="Enter your web url">
                        </div>
                        <div class="form-group">
                            <label for="typeDescription">Biography</label>
                            <textarea name="biography" id="typeDescription" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h4>Social Links:</h4>
                         <div class="form-group">
                             <div class="input-group">
                                <span disabled="" class="input-group-addon">
                                 https://www.facebook.com/
                                </span>
                                 <input type="text" name="facebook_profile" class="form-control" placeholder="Add your Facebook username (e.g. johnsmith).">
                             </div>
                         </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span disabled="" class="input-group-addon">
                                   https://twitter.com/
                                </span>
                                <input type="text" name="twitter_profile" class="form-control" placeholder="Add your Twitter username (e.g. johnsmith).">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span disabled="" class="input-group-addon">
                                  https://www.linkedin.com/
                                </span>
                                <input type="text" name="linkedin_profile" class="form-control" placeholder="Input your LinkedIn resource id (e.g. in/johnsmith).">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span disabled="" class="input-group-addon">
                                 https://www.skype.com/
                                </span>
                                <input type="text" name="skype_profile" class="form-control" placeholder="Add your skype username(e.g. johnsmith)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Status</label><br>
                            <label class="radio-inline"><input type="radio" name="status" value="active"> Active</label>
                            <label class="radio-inline"><input type="radio" name="status" value="inactive"> Inactive</label>
                            <label class="radio-inline"><input type="radio" name="status" value="suspended"> Suspended</label>
                        </div>
                        <div class="form-group">
                            <img style="height: 260px;width:240px;" src="http://via.placeholder.com/400x460" id="preview">

                            <div class="upload-box">
                                <label class="btn" style="background: gainsboro;width: 50%;">
                                    <input name="image" id="imgupload" style="display:none" type="file">
                                    <i class="fa fa-cloud-upload"></i> Upload Image
                                    <i class="fa fa-cog fa-spin  fa-fw margin-bottom" id="loaderIcon"
                                       style="color:green;margin-top:10px;display: none"></i>
                                </label>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <input type="submit" name="Save"  value="Submit" class="btn btn-success pull-right">
                    </div>
                    <div class="col-xs-6">
                        <a href="{!! route('teachers') !!}" class="btn btn-warning" onclick="return confirm('Are you confirm to cancel !')">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
@section('select2js')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bl182cv0ud67yxrbs9uor8xfscrz38s7bb5swlqoo5y4v7o6"></script>
    <script src="{!! asset('dist/js/select2.min.js') !!}"></script>
    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 150,
                menubar: false,
                plugins: [
                    'advlist autolink lists link image charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime media table contextmenu paste code help wordcount'
                ],
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tinymce.com/css/codepen.min.css']
            });
        })
    </script>
@endsection
@section('customJs')
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#preview').fadeIn(1000);
                    $('#preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgupload").change(function(){
            readURL(this);
        });

    </script>
@endsection